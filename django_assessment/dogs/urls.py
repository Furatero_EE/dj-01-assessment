from django.urls import path
from dogs import views

app_name = 'dogs'

urlpatterns = [
	path('view-dogs/', views.view_dogs, name='view_dogs'),
	path('profile/<int:dognum>', views.dog_profile, name='dog_profile'),
	path('add-dog/', views.add_dog, name='add_dog'),
]