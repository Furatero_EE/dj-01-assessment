from django.db import models

# Create your models here.

class Dog(models.Model):
	name = models.CharField(max_length=30)
	breed = models.CharField(max_length=30)
	age = models.CharField(max_length=3)
	weight = models.CharField(max_length=30)
	details = models.CharField(max_length=200)

	def __str__(self):
		return self.name

	def get_all(self):
		return [self.name, self.breed, self.age, self.weight, self.details]