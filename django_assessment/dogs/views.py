from django.shortcuts import render
from django.http import HttpResponse
from . models import Dog

# Create your views here.

def view_dogs(request):
	dogs = Dog.objects.all
	#return HttpResponse(dogs)
	return render(
		request,
		'view_dogs.html',
		{'dogs': dogs},
	)


def dog_profile(request, dognum):
	dog = Dog.objects.filter(id=dognum)
	return render(
		request,
		'dog_profile.html',
		{'dog': dog},
	)


def add_dog(request):
	if request.method == 'POST':
		dog_instance = Dog.objects.create(
			name = request.POST.get('name'),
			breed = request.POST.get('breed'),
			age = request.POST.get('age'),
			weight = request.POST.get('weight'),
			details = request.POST.get('details'),
		)
		return render(request, 'added_dog.html')

	return render(request, 'add_dogs.html')